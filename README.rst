=============
django-fakesi
=============

:author: Jesper Noehr

Fakesi (Fake ESI) provides the functionality to simulate server-side includes (SSI)
available in software such as nginx. This is primarily useful for developing locally
while still using SSI.

Fakesi is BSD licensed, and requires little more than the included middleware.
It also has an ``ssi`` templatetag, but that's entirely optional.

Installing
==========

Add ``fakesi`` to your ``INSTALLED_APPS`` setting, and add ``fakesi.middleware.FakesiMiddleware``
to your ``MIDDLEWARE_CLASSES`` settings (it must be before ``django.middleware.common.CommonMiddleware``).

Using
=====

It's very simple to use: Either stick ``<!--# include virtual="/some_url" -->`` in your templates,
or use the included ``{% ssi "some_url" %}`` template tag. Fakesi will handle the rest.

Fakesi also adds two methods to every request:

* ``is_ssi``, which will return True, if the request is coming through an SSI include.
* ``is_nginx``, which will return True, if the server sends "Nginx-SSI: On" (add with ``proxy_set_header Nginx-SSI on;`` in your nginx config.)
