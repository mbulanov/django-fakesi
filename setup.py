#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name='django-fakesi',
    version='1.0.0-alpha',
    description='Fake ESI for Django.',
    author='Jesper Noehr',
    author_email='jesper@noehr.org',
#    long_description=open('README.rst', 'r').read(),
    url='https://bitbucket.org/jespern/django-fakesi',
    packages=find_packages(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Utilities'
    ],
    test_suite='fakesi.tests.runtests.runtests',
)